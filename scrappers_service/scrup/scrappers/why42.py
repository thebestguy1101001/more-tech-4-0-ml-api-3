import requests
from bs4 import BeautifulSoup
import json


def none_check(obj):
    return obj != None


def get_soup(url):
    req = requests.get(url)
    return BeautifulSoup(req.text)


def get_categories(url, outside_class, inside_class, ind1, ind2,
                   outside_tag=False, inside_tag=False, flag=False):
    if flag:
        return [("Новости", url)]
    bs = get_soup(url)
    data = bs.find(attrs={"class": outside_class}) if not outside_tag else bs.find(outside_tag,
                                                                                   {"class": outside_class})
    categorical = data.find_all(attrs={"class": inside_class})[ind1:ind2] if not inside_tag else bs.find_all(
        inside_tag, {"class": inside_tag})[ind1:ind2]
    res = []
    for i in categorical:
        temp = i.find("a")
        href = temp["href"]
        text = temp.text
        res.append((text, href))

    return res


def get_articles(url, cls_name, cls_tag=False):
    """"""
    bs = get_soup(url)
    articles = bs.find_all(attrs={"class": cls_name}) if not cls_tag else bs.find_all(cls_tag, {"class": cls_name})

    return [art["href"] for art in articles]


def get_data(url, main_tag, title_class, tag_class, time_class, text_class, img_class,
             title_tag=False, tag_tag=False, time_tag=False, text_tag=False, img_tag=False):
    """put data from article"""

    bs = get_soup(url)

    title_pre = bs.find(attrs={"class": title_class}) if not title_tag else bs.find(title_tag, {"class": title_class})
    tag_pre = bs.find(attrs={"class": tag_class}) if not tag_tag else bs.find(tag_tag, {"class": tag_class})
    time_pre = bs.find(attrs={"class": time_class}) if not time_tag else bs.find(time_tag, {"class": time_class})
    text_pre = bs.find(attrs={"class": text_class}) if not text_tag else bs.find(text_tag, {"class": text_class})
    img_pre = bs.find(attrs={"class": img_class}) if not img_tag else bs.find(img_tag, {"class": img_class})


    title = title_pre.text if none_check(title_pre) else None
    tag = tag_pre.text if none_check(tag_pre) else None
    time = time_pre.text if none_check(time_pre) else None
    text = " ".join([i.text for i in text_pre.find_all("p")]) if none_check(time_pre) else None
    img = img_pre if none_check(time_pre) else None

    dct = {"title": title, "tags": [main_tag, tag], "time": time, "text": text, "img": img}

    return dct


def scrap(site_url, attrs_layer_1, attrs_layer_2, attrs_layer_3):
    """"""
    count = 0
    categories = get_categories(site_url, *attrs_layer_1)
    data = []

    for cat_name, cat_url in categories:
        hrefs = get_articles(cat_url, *attrs_layer_2)
        for href in hrefs:
            print(f"{count}")

            count += 1
            site_info = get_data(href, cat_name, *attrs_layer_3)
            data.append(site_info)

    print("All is well!")
    return data


if __name__ == "__main__":
    print("NO")

import csv

import pandas as pd
import re
import numpy as np
import gensim
import nltk

nltk.download('stopwords')

from nltk.corpus import stopwords
import pymorphy2

import json
import nltk
import ssl

try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    pass
else:
    ssl._create_default_https_context = _create_unverified_https_context

nltk.download('stopwords')
stop = stopwords.words('russian')

morph = pymorphy2.MorphAnalyzer()

w2v_model_text = gensim.models.Word2Vec.load("w2v.model")


def lemmatize(text):
    words = text.split()  # разбиваем текст на слова
    res = list()
    for word in words:
        p = morph.parse(word)[0]
        res.append(p.normal_form)

    return res


# Соединить с парсером #
########################
df = pd.read_json('consultant.json')
df = df.dropna()
df['text_clean'] = df['text'].apply(lambda x: re.sub(r'[^\w\s]+|[\d]+', r'', x).strip())
df['text_clean'] = df['text_clean'].apply(lambda x: x.lower())
df['text_clean'] = df['text_clean'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stop)]))
df['clean_text_list_lem'] = df['text_clean'].apply(lambda x: lemmatize(x))
w2v_model_text.build_vocab(df['clean_text_list_lem'], update=True)
df['vector_text'] = df['clean_text_list_lem'].apply(lambda x: [w2v_model_text.wv[item] for item in x])
df['vector_text'] = df['vector_text'].apply(lambda x: np.average(x, axis=0))


def BIGDATA(df):
    df = df.dropna()
    df['text_clean'] = df['text'].apply(lambda x: re.sub(r'[^\w\s]+|[\d]+', r'', x).strip())
    df['text_clean'] = df['text_clean'].apply(lambda x: x.lower())
    df['text_clean'] = df['text_clean'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stop)]))
    df['clean_text_list_lem'] = df['text_clean'].apply(lambda x: lemmatize(x))
    w2v_model_text.build_vocab(df['clean_text_list_lem'], update=True)
    df['vector_text'] = df['clean_text_list_lem'].apply(lambda x: [w2v_model_text.wv[item] for item in x])
    df['vector_text'] = df['vector_text'].apply(lambda x: np.average(x, axis=0))
    # save w2v


BIGDATA(df)


#######################


def preparation(text):
    text = pd.Series(text)
    text = text.apply(lambda x: re.sub(r'[^\w\s]+|[\d]+', r'', x).strip())
    text = text.apply(lambda x: x.lower())
    text = text.apply(lambda x: ' '.join([word for word in x.split() if word not in (stop)]))
    text = text.apply(lambda x: lemmatize(x))
    w2v_model_text.build_vocab(text, update=True)
    return text


def vectorization(preparation):
    vector = preparation.apply(lambda x: [w2v_model_text.wv[item] for item in x])
    vector = vector.apply(lambda x: np.average(x, axis=0))
    return vector


def euc(text):
    vector = vectorization(preparation(text))[0]
    print('uik')
    df['euclidean_din'] = df.vector_text.apply(lambda x: np.sqrt(((x - vector) ** 2).sum()))
    # TTT['euclidean_din'] = TTT.vector_text.progress_apply(lambda x: np.sqrt(((x - vector)**2).sum()) )
    print(df)

    return df.nsmallest(7, 'euclidean_din')


def euclidean(text):
    df = (euc(text))
    res = []
    df = df[['title', 'tags', 'time', 'text', 'img', 'text_clean',
             'clean_text_list_lem']]

    for i in range(len(df)):
        dct = dict(df.iloc[i])
        res.append(dct)
    new_dct = {"lul": res}
    print(new_dct)
    st = str({"data":res})
    return {"data":res}


# csv_content = pd.read_csv('goooooooodTXT.csv')

def csv_to_json():
    with open("goooooooodTXT.csv", encoding='utf-8') as r_file:
        file_reader = csv.DictReader(r_file, delimiter=",")
        count = 0
        for row in file_reader:
            if count == 0:
                print(f'Файл содержит столбцы: {", ".join(row)}')
            print("AAA")
            print(row["title"])
            count += 1
        print(f'Всего в файле {count + 1} строк.')
